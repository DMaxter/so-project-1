#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "lib/commandlinereader.h"


#define MAXARGS 3
#define BUFFERSIZE 300

float MAXCHILDREN = INFINITY;
unsigned long ACTIVECHILDREN = 0;


int main(int argc, char **argv){
	
	int status, argsRead;
	int quit = 0;

	char buffer[BUFFERSIZE];
	char **argVector = malloc(sizeof(char *) * (MAXARGS + 1));

	pid_t PID;

	// Temporary file to store PID and return values of child processes
	FILE *fp = tmpfile();

	if(fp == NULL){
		perror(NULL);
		exit(1);
	}


	// Define the maximum number of child processes
	if(argc == 2){
		MAXCHILDREN = atol(argv[1]);

		if(MAXCHILDREN <= 0){
			printf("The maximum number of child processes must be a positive number\n");
			exit(1);
		}
	}else if(argc > 2){
		printf("Unknown parameters found\n");
		exit(1);
	}

	do{
		argsRead = readLineArguments(argVector, MAXARGS + 1, buffer, BUFFERSIZE);

		// Command chooser
		if(!strcmp("exit", argVector[0])){
			if(argsRead > 1){
				printf("Incorrect use of command exit!\n");
				printf("Usage: exit\n");
			}else{
				quit = 1;
			}
		}else if(!strcmp("run", argVector[0])){
			if(argsRead == 1){
				printf("You must give an input file!\n");
				continue;
			}else if(argsRead > 2){
				printf("Incorrect use of command run!\n");
				printf("Usage: run <inputfile>\n");
			}else if(ACTIVECHILDREN == MAXCHILDREN){
				// Wait for the termination of a child process and write the output
				// to a temporary file 
				
				//In case EINTR occurs
				while((PID = wait(&status)) == -1)
						;

				fprintf(fp, "%d %d\n", PID, status);
				ACTIVECHILDREN--;
			}

			PID = fork();
			ACTIVECHILDREN++;

			if(PID == 0){
				execl("./CircuitRouter-SeqSolver/CircuitRouter-SeqSolver", 
					"CircuitRouter-SeqSolver", argVector[1], NULL);
			}else if(PID == -1){
				perror("Failed to fork!");
			}
		}else{
			printf("Unknown command\n");
		}
	}while(!quit);

	// Place the file cursor at the beginning of the file
	if(fseek(fp, 0, SEEK_SET) != 0){
		perror(NULL);
		exit(1);
	}

	// Print the values in the temporary file
	while(fscanf(fp, "%d %d", &PID, &status) != EOF){
		printf("CHILD EXITED (PID=%d; return %s)\n", PID,
				WIFEXITED(status) && status == 0 ? "OK" : "NOK");
	}

	fclose(fp);

	// Wait until the end of all child processes
	while(ACTIVECHILDREN--){
		PID = wait(&status);

		// If EINTR occurs
		if(PID == -1){
			perror(NULL);
			// Call wait again
			ACTIVECHILDREN++;
		}else{
			printf("CHILD EXITED (PID=%d; return %s)\n", PID,
				WIFEXITED(status) && status == 0 ? "OK" : "NOK");
		}
	}

	printf("END.\n");
	free(argVector);

	return 0;
}
