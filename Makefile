# Makefile, versao 1
# Sistemas Operativos, DEI/IST/ULisboa 2018-19

SOURCES-SOLVER = CircuitRouter-SeqSolver/router.c CircuitRouter-SeqSolver/maze.c CircuitRouter-SeqSolver/grid.c CircuitRouter-SeqSolver/coordinate.c CircuitRouter-SeqSolver/CircuitRouter-SeqSolver.c
SOURCES-SOLVER+= lib/vector.c lib/queue.c lib/list.c lib/pair.c
SOURCES-SHELL = CircuitRouter-SimpleShell.c lib/commandlinereader.c
OBJS-SOLVER = $(SOURCES-SOLVER:%.c=%.o)
OBJS-SHELL = $(SOURCES-SHELL:%.c=%.o)
OBJS = $(OBJS-SOLVER) $(OBJS-SHELL)
CC   = gcc
CFLAGS =-Wall -std=gnu99 -I ./
LDFLAGS=-lm
TARGET-SOLVER = CircuitRouter-SeqSolver/CircuitRouter-SeqSolver
TARGET-SHELL = CircuitRouter-SimpleShell


all: $(TARGET-SOLVER) $(TARGET-SHELL)


$(TARGET-SOLVER): $(OBJS-SOLVER)
	$(CC) $(CFLAGS) $^ -o $(TARGET-SOLVER) $(LDFLAGS)

$(TARGET-SHELL): $(OBJS-SHELL)
	$(CC) $(CFLAGS) $^ -o $(TARGET-SHELL) $(LDFLAGS)


# CircuitRouter-SeqSolver
CircuitRouter-SeqSolver/CircuitRouter-SeqSolver.o: CircuitRouter-SeqSolver/CircuitRouter-SeqSolver.c CircuitRouter-SeqSolver/maze.h CircuitRouter-SeqSolver/router.h lib/list.h lib/timer.h lib/types.h
CircuitRouter-SeqSolver/router.o: CircuitRouter-SeqSolver/router.c CircuitRouter-SeqSolver/router.h CircuitRouter-SeqSolver/coordinate.h CircuitRouter-SeqSolver/grid.h lib/queue.h lib/vector.h
CircuitRouter-SeqSolver/maze.o: CircuitRouter-SeqSolver/maze.c CircuitRouter-SeqSolver/maze.h CircuitRouter-SeqSolver/coordinate.h CircuitRouter-SeqSolver/grid.h lib/list.h lib/queue.h lib/pair.h lib/types.h lib/vector.h
CircuitRouter-SeqSolver/grid.o: CircuitRouter-SeqSolver/grid.c CircuitRouter-SeqSolver/grid.h CircuitRouter-SeqSolver/coordinate.h lib/types.h lib/vector.h
CircuitRouter-SeqSolver/coordinate.o: CircuitRouter-SeqSolver/coordinate.c CircuitRouter-SeqSolver/coordinate.h lib/pair.h lib/types.h
lib/vector.o: lib/vector.c lib/vector.h lib/types.h lib/utility.h
lib/queue.o: lib/queue.c lib/queue.h lib/types.h
lib/list.o: lib/list.c lib/list.h lib/types.h
lib/pair.o: lib/pair.c lib/pair.h

# CircuitRouter-SimpleShell
CircuitRouter-SimpleShell.o: CircuitRouter-SimpleShell.c lib/commandlinereader.h
lib/commandlinereader.o: lib/commandlinereader.c lib/commandlinereader.h

$(OBJS):
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	@echo Cleaning...
	rm -f $(OBJS) $(TARGET-SHELL) $(TARGET-SOLVER)


